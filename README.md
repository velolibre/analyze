## Goals

  * a mechanism/platform for
    * dumping activity files into some sort of persistent storage
    * provide a logical/aggregate layer on top of those files
    * visually querying/analyzing them

  * ideas:
    * it's unclear to me upfront what the schema of activities *is*, and since canonically this stuff is stored in denormazized documents *anyway*, might as well lean into this and use a document store rather than a relational database
    * Write a small TCX/GPX/FIT -> JSON tool, deploy as a serverless function that takes a file and pushes to MongoDB
      * doing this losslessly would be nice, but probably not required
    * Try MongoDB 'Compose' and Metabase as tools for analyzing/querying over Mongo
    * investigate the possible use of tools like dbt for managing logic layers on top of the underyling document store
      * seems like... not
